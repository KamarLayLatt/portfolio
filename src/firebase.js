// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAhyghFsCyj1PSZEH71D-HpRnBPCjP5vwY",
  authDomain: "portfolio-a15fb.firebaseapp.com",
  projectId: "portfolio-a15fb",
  storageBucket: "portfolio-a15fb.firebasestorage.app",
  messagingSenderId: "444308576589",
  appId: "1:444308576589:web:de37b107a2e74f5da57f04"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
