import Image from "next/image";
import AnchorLink from "react-anchor-link-smooth-scroll";

export default function Landing() {
  return (
    <div className=" sm:flex sm:flex-row-reverse bg-slate-0">
      <div className="sm:w-1/2">
        <div className="grid place-content-center h-full">
          <div
            className="relative"
            style={{ height: "340px", width: " 340px" }}
          >
            <Image
              src="/images/profile.jpg"
              alt="profile"
              fill={true}
              style={{ objectFit: "cover", borderRadius: "5%" }}
            />
          </div>
        </div>
      </div>
      <div className="sm:w-1/2 mt-4">
        <div className="flex flex-col">
          <div
            className="opacity-60 font-semibold"
            style={{ fontSize: "20px", color: "#C39595" }}
          >
            HELLO
          </div>
          <div className="text-primary mt-3" style={{ fontSize: "35px" }}>
            I&apos;m <span className="font-bold">Saw Kamar Lay Latt</span>
          </div>
          <div className="text-primary mt-3" style={{ fontSize: "35px" }}>
            {/* <span className="font-bold">a</span> */}
            Web Developer
          </div>
          <div className="mt-4" style={{ fontSize: "20px" }}>
            I am a web developer with {new Date().getFullYear() - 2018} years of
            experience in the field. I possess the skills and expertise to
            assist you with your business needs.
          </div>
          <div className="flex space-x-4 mt-8">
            <AnchorLink href="#contact">
              <div className="bg-primary text-white px-4 py-2 rounded-lg cursor-pointer">
                Hire Me
              </div>
            </AnchorLink>
            <AnchorLink href="#about">
              <div className=" px-4 py-2 rounded-lg border-2 border-primary cursor-pointer">
                Portfolio
              </div>
            </AnchorLink>
          </div>
        </div>
      </div>
    </div>
  );
}
