import Image from "next/image";
import { collection, addDoc } from "firebase/firestore";
import { db } from "@/firebase";
import { useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";

export default function Contact() {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [message, setMessage] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const submit = async (e) => {
    try {
      e.preventDefault();

      setIsLoading(true);
      axios
        .post("/api/slack", {
          text: `A new contact comming (${name}) \n==========================\n *name*: ${name} \n *email*: ${email} \n *message*: ${message}`,
        }).then(() => {
          setIsLoading(false);
        })
        .catch((e) => {
          console.log("slack error => ", e);
          setIsLoading(false);
        });
      // const docRef = await addDoc(collection(db, "contacts"), {
      //   name,
      //   email,
      //   message,
      // })
      //   .then(() => {
      //     setIsLoading(false);
      //     setName("");
      //     setEmail("");
      //     setMessage("");
      //     Swal.fire({
      //       title: "Success",
      //       text: "Successfully send your message",
      //       icon: "success",
      //       confirmButtonText: "OK",
      //     });
      //     axios
      //       .post("/api/slack", {
      //         text: `A new contact comming (${name}) \n==========================\n *name*: ${name} \n *email*: ${email} \n *message*: ${message}`,
      //       })
      //       .catch((e) => {
      //         console.log("slack error => ", e);
      //       });
      //   })
      //   .catch((e) => {
      //     Swal.fire({
      //       title: "Error",
      //       text: "Sorry, something wrong in submitting",
      //       icon: "error",
      //       confirmButtonText: "OK",
      //     });
      //     setIsLoading(false);
      //   });
    } catch (e) {
      console.error("Error adding document: ", e);
      Swal.fire({
        title: "Error",
        text: "Sorry, something wrong in submitting",
        icon: "error",
        confirmButtonText: "OK",
      });
      setIsLoading(false);
    }
  };
  return (
    <div>
      <div className="mt-4" style={{ fontSize: "25px" }}>
        <div className="sm:flex">
          <div className="sm:w-1/2">
            <div style={{ fontSize: "40px" }}>
              Contact <span className="text-primary font-semibold">Me!</span>
            </div>
            <div className="mt-16 flex flex-col space-y-14 font-semibold text-xl">
              <div className="flex items-center space-x-4">
                <div>
                  <Image
                    src="/icons/phone.svg"
                    alt="phone"
                    width={33}
                    height={33}
                  />
                </div>
                <div>+959263683288</div>
              </div>
              <div className="flex items-center space-x-4">
                <div>
                  <Image
                    src="/icons/mail.svg"
                    alt="phone"
                    width={33}
                    height={33}
                  />
                </div>
                <div>kamarehtha96@gmail.com</div>
              </div>
              <div className="flex items-center space-x-4">
                <div>
                  <Image
                    src="/icons/location.svg"
                    alt="phone"
                    width={33}
                    height={33}
                  />
                </div>
                <div>North Dagon Township, Yangon</div>
              </div>
            </div>
          </div>
          <div className="mt-14 sm:mt-0 sm:w-1/2">
            <form onSubmit={(e) => submit(e)}>
              <div className="flex flex-col space-y-8">
                <div>
                  <input
                    className="bg-primary rounded-lg ps-6 text-white w-full"
                    type="text"
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    placeholder="Name"
                    style={{ fontSize: "18px", height: "80px" }}
                    required
                  />
                </div>
                <div>
                  <input
                    className="bg-primary rounded-lg ps-6 text-white w-full"
                    type="email"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    placeholder="Your Mail"
                    style={{ fontSize: "18px", height: "80px" }}
                    required
                  />
                </div>
                <div>
                  <textarea
                    required
                    className="bg-primary rounded-lg ps-6 py-4 text-white w-full"
                    type="text"
                    onChange={(e) => setMessage(e.target.value)}
                    value={message}
                    placeholder="Message"
                    style={{
                      fontSize: "18px",
                      height: "180px",
                    }}
                  />
                </div>
              </div>
              <button
                type="submit"
                className="bg-primary text-white px-4 py-2 rounded-lg w-36 cursor-pointer mt-6 text-center"
                style={{ fontSize: "20px" }}
              >
                {!isLoading ? (
                  <span>Send</span>
                ) : (
                  <span className="animate-pulse">Sending ... </span>
                )}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
