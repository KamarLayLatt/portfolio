import Image from "next/image";
export default function Project() {
  const projects = [
    {
      name: "Thukha Application",
      image: "/images/projects/thukha.png",
      description:
        "Food delivery project that supports via the web or mobile phone orders. There are several functions, such as a point system, order cancellation, and point sharing with other users.",
    },
    {
      name: "Nagarrni Pay",
      image: "/images/projects/nagarrni.webp",
      description:
        "Nagarrni is a one-stop shop for consumers. Purchase bus tickets, pay bills, and top up phone bill.",
    },
    {
      name: "Universitylife",
      image: "/images/projects/universitylife.svg",
      description:
        "Universitylife is an ecommerce website for a phone case store. You may get a phone cover, and the DIY function is included.",
    },
    {
      name: "Taygita",
      image: "/images/projects/taygita.png",
      description:
        "Taygita is a platform that provide musical information. There are several blogs and an area for information sharing.",
    },
    {
      name: "Octillion",
      image: "/images/projects/octillion.png",
      description:
        "Users may purchase and sell NFT tokens on the website Octillion.",
    },
    {
      name: "UCSYOQ",
      image: "/images/projects/ucsyoq.webp",
      description:
        "Students attending UCSY schools can use UCSY OQ. In this application, students may research prior exams and submit any queries. In this application, quizzes are available.",
    },
  ];
  return (
    <div>
      <div className="text-center font-semibold" style={{ fontSize: "35px" }}>
        Projects
      </div>
      <div className="mt-4" style={{ fontSize: "20px" }}>
        <div
          className="grid grid-cols-1 sm:grid-cols-2 
        lg:grid-cols-3 gap-4"
        >
          {projects.map((project, i) => (
            <div
              style={{
                backgroundColor: "#EDEDED",
                height: "380px",
              }}
              className="w-full rounded-lg p-2"
              key={i}
            >
              <div className="flex flex-col space-y-2">
                <div className="w-full h-52 relative">
                  <Image
                    src={project.image}
                    alt="profile"
                    fill={true}
                    style={{ objectFit: "cover", borderRadius: "2%" }}
                  />
                </div>
                <div className="font-semibold">{project.name}</div>
                <div className="">
                  <p className="line-clamp-4 text-lg indent-2">
                    {project.description}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
