export default function About() {
  return (
    <div>
      <div className="text-center font-semibold" style={{ fontSize: "35px" }}>
        About Me
      </div>
      <div className="mt-4 indent-10" style={{ fontSize: "20px" }}>
        I am a web developer with a Bachelor of Computer Science, my expertise
        lies in the realms of CSS, React, Vue, PHP, Mysql and Laravel. Having
        embarked on this professional journey in 2018, I have been passionately
        honing my skills and knowledge in these technologies ever since. CSS
        allows me to effortlessly style and design websites, ensuring a visually
        pleasing user experience. The proficiency in React enables me to build
        interactive and dynamic user interfaces, while Vue empowers me with its
        simplicity and ease of use for frontend development. With a solid
        command over PHP and Laravel framework at my disposal, I am equipped to
        develop robust backend solutions that seamlessly integrate with frontend
        functionalities. This diverse skill set has enabled me to tackle various
        web development projects with confidence and creativity.
      </div>
    </div>
  );
}
