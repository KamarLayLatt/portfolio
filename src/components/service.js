export default function Service() {
  const services = [
    {
      name: "Web Design",
      description:
        "I specialize in providing a wide range of web development designs tailored to your specific needs. With my expertise in utilizing tools such as Adobe XD and Figma UI, I can create visually stunning and user-friendly interfaces.",
    },
    {
      name: "Front End",
      description:
        "I am proficient in developing websites using Vue, React, Sass, and Tailwind CSS, which are highly regarded UI frameworks. By leveraging these technologies, I ensure that your website not only looks professional but also functions seamlessly.",
    },
    {
      name: "Backend",
      description:
        "I specialize in providing backend services, including storage, database management, and data maintenance. My expertise lies in developing applications using PHP, Laravel, Mysql and Node.js. With my skills, I can ensure efficient and reliable backend operations for your project.",
    },
  ];
  return (
    <div>
      <div className="text-center font-semibold" style={{ fontSize: "35px" }}>
        My Services
      </div>
      <div className="mt-4" style={{ fontSize: "25px" }}>
        <div className="flex flex-col space-y-2 sm:flex-row sm:space-x-4 sm:space-y-0">
          {services.map((service, i) => (
            <div
              key={i}
              style={{
                height: "320px",
                backgroundColor: "#EDEDED",
              }}
              className="w-full sm:w-1/3 p-4 text-ellipsis overflow-hidden"
            >
              <div className="font-semibold" style={{ fontSize: "20px" }}>
                {service.name}
              </div>
              <div className="mt-8" style={{ fontSize: "18px" }}>
                {service.description}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
