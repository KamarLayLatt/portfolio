import Landing from "@/components/landing";
import About from "@/components/about";
import Service from "@/components/service";
import Contact from "@/components/contact";
import Project from "@/components/project";
import Head from "next/head";

export default function Home() {
  return (
    <div className="px-5 sm:px-20 py-10">
      <Head>
        <title>Saw Kamar Lay Latt</title>
        <link rel="shortcut icon" href="/images/profile.jpg" />
      </Head>
      <section id="landing">
        <Landing />
      </section>
      <section id="about" className="mt-32">
        <About />
      </section>
      <section id="service" className="mt-32">
        <Service />
      </section>
      <section id="project" className="mt-32">
        <Project />
      </section>
      <section id="contact" className="mt-32">
        <Contact />
      </section>
    </div>
  );
}
