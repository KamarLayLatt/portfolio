const axios = require("axios");

const slackWebhookHandler = async (req, res) => {
  if (req.method === "POST") {
    try {
      const slackWebhookUrl =
        "https://hooks.slack.com/services/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";
      const response = await axios.post(slackWebhookUrl, req.body);
      res.status(response.status).json(response.data);
    } catch (error) {
      res.status(500).json({
        error: "An error occurred while sending the request to Slack.",
      });
    }
  } else {
    res.status(405).json({ error: "Method not allowed." });
  }
};

export default slackWebhookHandler;
